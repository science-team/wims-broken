2_shape:Figura en dimensi�n 2
2d_arrays:matrices 2D
3_shape:Figura en dimensi�n 3
abelian_group:Grupo abeliano
absolute_frequency:Frecuencia absoluta
absolute_value:Valor absoluto
abstract_algebra:�lgebra abstracta
acceleration:Aceleraci�n
actions:Acciones
active_site:Centro activo
adaptative_immunity:Inmunidad adaptativa
addition:Adici�n
affine_function:Funci�n af�n
affine_geometry:Geometr�a af�n
algebra:�lgebra
algebra_geometry:�lgebra geom�trica
algebraic_fraction:Fracci�n algebraica
algebraic_geometry:Geometr�a algebraica
algorithmics:Algoritmia
altitude:Altitud
american_civilisation:Civilizaci�n americana
amino_acid:Amino�cido
analysis:An�lisis
analytic_geometry:Geometr�a anal�tica
ancient_greek:Grecia antigua
anesthetic_drug:Droga anest�sica
angle_bisector:Bisectriz
angles:�ngulos
animal_cell:C�lula animal
animal_tissue:Tejido animal
antibody:Anticuerpo
antibody_diversity:Diversidad de anticuerpos
antigen:Ant�geno
antiseptic_drug:Droga antis�ptica
area:�rea
arithmetic:Aritm�tica
arithmetic_sequence:Progresi�n aritm�tica
arrays:Arreglos
arts:Artes
association_rate:Tasa de asociaci�n
asymptote:As�ntota
atom:�tomo
bacteria:Bacteria
barycenter:Baricentro
basis:Base
basis_change:Cambio de base
between_variance:Inter-varianza
bezout_algorithm:Algoritmo de Bezout
bilinear_algebra:�lgebra bilineal
binomial:Binomial
binomial_distribution:Distribuci�n binomial
binomial_test:Prueba binomial
biochemistry:Bioqu�mica
biology:Biolog�a
body_system:Sistema corporal
bonds:Enlaces
botany:Bot�nica
bound:Cotas
boxplot:Diagrama de caja
business_english:Ingl�s de negocios
butterfly:Mariposa
calculation:C�lculo
calculus:C�lculo
calendar:Calendario
canonical_form:Forma can�nica
capacitor:Condensador
catalysis:Cat�lisis
cell_biology:Biolog�a celular
cell_cycle:Ciclo celular
cell_death:Muerte celular
cell_differentiation:Diferenciaci�n celular
cell_division:Divisi�n celular
cercl_a1:CERCL A1
cercl_a2:CERCL A2
cercl_b1:CERCL B1
cercl_b2:CERCL B2
cercl_c1:CERCL C1
cercl_c2:CERCL C2
characteristic_polynomial:Polinomio caracter�stico
chemical_equilibrium:Equilibrio qu�mico
chemical_reaction:Reacci�n qu�mica
chemical_synthesis:S�ntesis qu�mica
chemistry:Qu�mica
chisquare_test:Prueba chi-cuadrado
chromosome:Cromosoma
cinematics:Cinem�tica
circle:C�rculo
circle_equation:Ecuaci�n del c�rculo
circles:C�rculos
circuit_law:Ley de circuito
circulation:Circulaci�n
civic_education:Educaci�n c�vica
class_i:Clase I
class_ii:Clase II
classical_antiquity:Antig�edad cl�sica
clock:Reloj
clonal_selection:Selecci�n clonal
clthm:Teorema del l�mite central
clustering:Algoritmos de clasificaci�n
code:Encriptaci�n
coding:Teor�a de la encriptaci�n
collections:Colecciones
color:Color
combination:Combinaci�n
combinatorics:Combinatoria
combustion:Combusti�n
common_ancestor:Ancestro com�n
complement_system:Sistema complementario
complementary_subspace:Subespacio complementario
complex_analysis:Variable compleja
complex_number:N�meros complejos
complex_plane:Plano complejo
complexity:Complejidad
composite_types:Tipos compuestos
cond_instruction:Instrucciones condicionales
conditional_frequency:Frecuencia condicional
conditional_probability:Probabilidad condicional
cone:Cono
confidence_interval:Intervalo de confianza
congruence:Congruencia
conics:C�nicas
connective_tissue:Tejido conectivo
contingency_table:Tabla de contingencia
continued_fraction:Fracci�n continua
continuity:Continuidad
continuous_probability_distribution:Distribuci�n de probabilidad continua
contraposition:Contrarrec�proca
contrast_agent:Agente de contraste
contribution_variance:Contribuci�n a la varianza
convergence:Convergencia
conversion:Conversi�n
convexity:
cooperativity:Cooperatividad
coordinates:Coordenadas
corporate_finance:Finanzas coorporativas
correlation:Correlaci�n
covariance:Covarianza
cram:Representaci�n de Cram
critical_point:Punto cr�tico
cross_product:Producto cruz
cryptology:Criptolog�a
crystallography:Cristalograf�a
cube:Cubo
cubic_crystal_system:Sistema cristalino c�bico
cumulative_distribution:Funci�n de distribuci�n acumulativa
cumulative_frequency:Frecuencia acumulada
curl:Rotor
curve_fitting:Ajuste de curvas
curves:Curvas
cyclic_code:C�digo c�clico
cyclic_group:Grupo c�clico
cylinder:Cilindro
cytokine:Citocina
data_analysis:Datos estad�sticos
data_gestion:Gesti�n de datos
data_structures:Estructuras de datos
decile:Decil
decimals:Decimales
definite_integral:Integrales definidas
dendritic_cell:C�lulas dendr�ticas
density:Densidad
derivative:Derivadas
descriptive_statistics:Estad�stica descriptiva
determinant:Determinante
development_of_lymphocytes:Desarrollo de linfocitos
diabete_drug:Medicamento para la diabetes
diacritical_mark:Signo diacr�tico
diagonalization:Diagonalizaci�n
differentiability:Derivabilidad
differential_equation:Ecuaci�n diferencial
differential_equations:Ecuaciones diferenciales
differential_geometry:Geometr�a diferencial
differential_system:Sistema diferencial
dimension:Dimensi�n
dimensional_analysis:An�lisis dimensional
diophantine_equations:Ecuaciones diofantinas
dirac_delta_function:Funci�n delta de Dirac
direction:Direcci�n
discrete_logarithm:Logaritmo discreto
discrete_mathematics:Matem�ticas discretas
discrete_probability_distribution:Distribuci�n de probabilidad discreta
distance:Distancia
distance_method:M�todo de la distancia
divergence:Divergencia
diversity:Diversidad
divisibility:Divisibilidad
division:Divisi�n
dna:ADN
domain:Dominio
dynamic_system:Sistemas din�micos
dynamics:Din�mica
earth_sciences:Ciencias de la tierra
ecology:Ecolog�a
economics:Econom�a
electrical_engineering:Ingenier�a el�ctrica
electricity:Electricidad
electronics:Electr�nica
electrophoresis:Electroforesis
electrostatics:Electroest�tica
elementary_algebra:�lgebra elemental
elementary_calculus:C�lculo elemental
elementary_geometry:Geometr�a elemental
elementary_mathematics:Matem�ticas elementales
ellipse:Elipse
emphasis:�nfasis
encryption:Cifrado
endomorphism_reduction:Reducci�n de endomorfismo
energy:Energ�a
english:Ingl�s
english_vocabulary:Vocabulario de ingl�s
enthalpy:Entalp�a
entropy:Entrop�a
enzyme:Enzima
epithelial_tissue:Tejido epitelial
equation_systems:Sistemas de ecuaciones
equations:Ecuaciones
error_correcting_codes:C�digo corrector de errores
error_detection:Detecci�n de errores
estimation:Estimaci�n 
etymology:Etimolog�a
eucaryote:Eucariota
euclidean_algorithm:Divisi�n euclidiana
euclidean_cloud:
euclidean_geometry:Geometr�a euclidiana
eulerian_graph:Grafo euleriano
events:Eventos
evolution:Evoluci�n
expectation:Esperanza
exponent:Exponente
exponential:Exponencial
exponential_distribution:Distribuci�n exponencial
extremum:Extremo
factorial:Factorial
factorization:Factorizaci�n
fc_receptor:Receptor FC
field:Cuerpos
financial_analysis:An�lisis financiero
financial_market:Mercado financiero
financial_mathematics:Matem�ticas financieras
finite_field:Cuerpo finito
finite_group:Grupo finito
finnish:Finland�s
flow_cytometry:Citometr�a de flujo
fluid_mechanics:Mec�nica de flu�dos
fourier_series:Series de Fourier
fourier_transform:Transformadas de Fourier
fraction:Fracciones
french:Franc�s 
french_conjugation:Conjugaci�n en franc�s
french_grammar:Gram�tica francesa
french_reading:Lectura en franc�s
french_spelling:Escritura en franc�s
french_vocabulary:Vocabulario franc�s
frieze:
function_variation:Variaci�n de una funci�n
functional_group:Grupo funcional
functions:Funciones
gas_mechanics:Mec�nica de gases
gauss_algorithm:Algoritmo de Gauss
gaz_mechanics:Mec�nica de gases
gcd_lcm:mcd y mcm
gene_rearrangement:Reordenamiento de genes
genetics:Gen�tica
geography:Geograf�a
geometric_distribution:
geometric_sequence:Progresi�n geom�trica
geometric_vocabulary:Vocabulario geom�trico
geometry:Geometr�a
german:Alem�n
goldbach:Goldbach
gradient:Gradiente
grammar:Gram�tica
graph:Grafos
graph_coloring:Coloreo de grafos
graphing:Graficando
greek:Griego
group:grupos
group_action:Acciones de grupos
group_theory:Teor�a de grupos
hamming:Distancia de Hamming
hematopoiesis:Hematopoyesis
hierarchical_cluster_analysis:An�lisis cl�ster jer�rquico
histocompatibility:Histocompatibilidad
histogram:Histograma
histology:Histolog�a
history:Historia
homography:Homograf�as
homology:Homolog�a
homonymy:Homonimia
homothety:
hyperbola:Hip�rbola
hyperplane:Hiperplanos
ideal_gas:Gas ideal
image:Imagen
immunoglobulines:Inmunoglobulinas
immunology:Inmunolog�a
imperative_programming:Programaci�n imperativa
implication:Implicancia
independent_events:Eventos independientes
independent_random_variables:Variables aleatorias independientes
inductance:Inductancia
inequalities:Desigualdades
inequations:Inecuaciones
infection:Infecci�n
inferential_statistics:Estad�stica inferencial
infonet:Tecnolog�as de la informaci�n y la comunicaci�n
informatics:Inform�tica
information_theory:Teor�a de la informaci�n
inhibitor:Inhibidor
injectivity:Inyectividad
innate_immunity:Inmunidad innata
inorganic_chemistry:Qu�mica inorg�nica
integers:Enteros
integral:Integrales
interferon:Interfer�n
interpolation:Interpolaci�n
intervals:intervalos
inverse_function:Funci�n inversa
ionisation:ionizaci�n
ioput:Input/Output
irreducible_fraction:Fracci�n irreducible
isometries:Isometr�as
kinetics:Cin�tica
language:Lenguaje
laplace_distribution:
laplace_transforms:Transformada de Laplace
latin:Lat�n
length:Largo
leucocyte:Leucocito
level_set:Conjunto de nivel
lewis:Estructura de Lewis
lexical_field:Campo l�xico
limit:L�mites
line:Recta
line_equation:Ecuaci�n de la recta
line_integral:Integral de l�nea
linear_algebra:�lgebra lineal
linear_differential_equation:Ecuaci�n diferencial lineal
linear_function:Funci�n lineal
linear_maps:Transformaciones lineales
linear_optimisation:Optimizaci�n lineal
linear_system:Sistema lineal
linear_systems:Sistemas lineales
lines:rectas
linguistics:Ling��stica
lissajous:Curvas de Lissajous
listening:
literal_calculation:C�lculo literal
literature:Literatura
logarithm:Logaritmo
logic:L�gica
logic_recreation:Juegos l�gicos
loops:Instrucciones iterativas
lymphocyte:Linfocito
lymphoid_organ:�rganos linfoides
macrophage:Macr�fago
magnetism:Magnetismo
management:Administraci�n
maps:Aplicaciones
markov:Markov
markov_chains:Cadenas de Markov
masse:Masa
math_symbols:S�mbolos matem�ticos
mathematics:Matem�ticas
matrix:matrices
maximum_flow:Flujo m�ximo
maximum_parsimony:M�xima parsimoni
mean:Media
measurement:Medici�n
mechanics:Mec�nica
medecine:Medicina
median:Mediana
median_line:L�nea mediana
medicinal_chemistry:Qu�mica medicinal
mental_calculation:C�lculo mental
methodical_calculation:C�lculo met�dico
methodology:Metodolog�a
mhc:MHC
microbe:Microbio
midpoint:Punto medio
misc:Miscel�neo
mitosis:Mitosis 
modelling:Modelamiento
modular_arithmetic:Aritm�tica modular
module:M�dulo
molecular_biology:Biolog�a molecular
molecule:Mol�cula
monophyletic_group:Grupo monofil�tico
month_week:Meses y semanas
multiplication:Multiplicaci�n
multivariable_function:Funciones de varias variables
muscle_tissue:Tejido muscular
music:M�sica
mycology:Micolog�a
negation:Negaci�n
nervous_tissue:Tejido nervioso
neutrophil:Neutr�filos
nk_cell:C�lulas NK
normal_distribution:Distribuci�n normal
normal_vector:Vector normal
nsaids:Nsaids
number:N�meros
number_line:Recta num�rica
number_theory:Teor�a de n�meros
number_writing:Escritura num�rica
numbering_system:Sistemas de numeraci�n
numeration:Numeraci�n
numerical_analysis:An�lisis num�rico
numerical_integration:Integraci�n num�rica
numerical_method:M�todos num�ricos
ode:EDO
ohm_law:Ley de Ohm
oper_prec:Precedencia de operadores
operation:Operaci�n
operational_research:Investigaci�n de operaciones
optics:�ptica
order:Orden 
organelle:Org�nulo
organic_chemistry:Qu�mica org�nica
oscillators:Osciladores
pain_drug:Medicamentos para el dolor
parabola:Par�bola
parallel:Paralelo
parallelogram:Paralel�gramo
parametric_curves:Curvas param�tricas
parametric_surfaces:Superficies param�tricas
partial_derivative:Derivada parcial
passive_form:Voz pasiva
past_tenses:Los tiempos del pasado
pbsolving:Resoluci�n de problemas
peptide:P�ptido
percentage:Porcentajes
percents:Porcentaje
perimeter:Per�metro
periodic_table:Tabla peri�dica
permutation:Permutaci�n
perpendicular_bisector:Bisectriz perpendicular
pert:PERT
phagocytosis:Fagocitosis
pharmocophore:Farmac�foro
phase_changes:Cambios de fase
phi2:Coeficiente de contingencia cuadr�tica media
phonetics:Fon�tica
phylo_homology:Homolog�a
phylogenetic_tree:�rbol filogen�tico
phylogenetics:Filogen�tica
physical_education:Educaci�n f�sica
physics:F�sica
plane:Planos
plane_equation:Ecuaci�n del plano
plane_section:Secci�n de un plano
plant:Planta
plant_cell:C�lula vegetal
poisson_distribution:Distribuci�n de Poisson
polar_curves:Curvas polares
polygons:Pol�gonos
polyhedron:Poliedros
polynomials:Polinomios
population_genetics:Gen�tica poblacional
potential:Potencial
power:Potencias
power_series:Series de potencias
prealg:Pre-�lgebra
precalc:Pre-c�lculo
prediction_interval:Intervalo de predicci�n
preimage:Preimagen
pression:Presi�n
primes:N�meros primos
primitive:Primitivas
priority_rule:Reglas de prioridad
prism:Prisma
prob_graph:Gr�fo probabil�stico
probability:Probabilidad
probability_distribution:Distribuciones
probability_generating_function:Funci�n generadora
probability_space:Espacio de probabilidad
probability_statistics:Probabilidad-estad�stica
procedures:Procedimientos
process_engineering:Ingenier�a de Procesos
prog_functions:Funciones de programaci�n
programming:Programaci�n
pronunciation:Pronunciaci�n
proportionality:Proporcionalidad
protein:Prote�na
protractor:Transportador
pyramid:Pir�mide
quadratic_form:Forma cuadr�tica
quadrilateral:Cuadril�tero
quantifier:Cuantificador
quantile:Cuantil
quartile:Cuartil
quotient_group:Grupo cuociente
random:Aleatorio
random_variable:Variable aleatoria
random_vector:Vector aleatorio
range_kernel:Rango y n�cleo
rank:Rango
rational_number:N�mero racional
ray_optics:Rayos �pticos
reading:Lectura
real_function:Funci�n real
real_number:N�meros reales
reasoning:Razonamiento
receptor:Receptor
rectangles:Rect�ngulos
recurrence_relation:Relaci�n de recurrencia
recursive_functions:Funci�n recursiva
reference_function:Funciones de referencia
regression:An�lisis de la regresi�n
relative_number:N�mero relativo
relativity:Relatividad
resistance:Resistencia
ring:Anillos
riskreturn:Riesgo y retorno
road_traffic_safety:Seguridad vial
root_tree:�rbol con ra�z
roots:Ra�ces
rotation:Rotaci�n
ruler_and_compass:Regla y comp�s
rv_convergence:Convergencia de variables aleatorias
sampling:Muestreo
sampling_distribution:Distribuci�n muestral
scalar_product:Producto escalar
scheduling:Proyecto de programaci�n
sciences:Ciencias
scientific_notation:Notaci�n cient�fica
selection_of_lymphocytes:Selecci�n de linfocitos
sense:Sentido
sentence_stress:Entonaci�n
sequence:Secuencias
series:Series
set_theory:Teor�a de conjuntos
sets:Conjuntos
signal_processing:Procesamiento de se�ales
signaling_pathway:V�a de se�alizaci�n
signdigits:
similarities:Similaridades
simplex_method:Simplex
simulation:Simulaci�n
solid_geometry:Geometr�a de s�lidos
spatial_skill:Habilidades espaciales
speaking:Comunicaci�n oral
specificity:Especificidad
spelling:Escritura
sphere:Esfera
sports:Deportes
spreadsheet:Hoja de c�lculo
squareroot:Ra�z cuadrada
standard_deviation:Desviaci�n est�ndar
static_register:Registro est�tico
statistical_test:Prueba estad�stica
statistics:Estad�stica
steroid:Esteroides
stokes_thm:Teorema de Stokes
struct:Estructura
student_test:Prueba student
subst_integral:Integraci�n por sustituci�n
subtraction:Sustracci�n
surfaces:Superficies
surjectivity:Sobreyectividad
sustainable_dev:Desarrollo sostenible
symmetric_group:Grupo sim�trico
symmetry:Simetr�a
t_cell_receptor:Receptor de c�lulas T
tangent:Tangente
tangent_plane:Plano tangente
taylor_expansion:Desarrollo de Taylor
taylor_series:Series de Taylor
technical_language:Lenguaje t�cnico
tensor_field:Campo tensorial
thales:Tales
thermochemistry:Termoqu�mica
thermodynamics:Termodin�mica
tiling:Embaldosado
time:Tiempo
timeline:L�nea de tiempo
titration:Valoraci�n
topology:Topolog�a
toxin:Toxina
trajectory:Trayectoria
transformation_group:Grupo de transformaciones
transition_matrix:Matriz de transici�n
translation:Traslaci�n
tree_topology:Topolog�a de �rbol
triangles:Tri�ngulos
trigonometric_circle:C�rculo trigonom�trico
trigonometric_functions:Funciones trigonom�tricas
trigonometry:Trigonometr�a
trinomial:Trinomio
truth_table:Tabla de verdad
uniform_distribution:Distribuci�n uniforme
units_tests:
unroot_tree:�rbol sin ra�z
upper_bound:Cota superior
vaccine:Vacuna
variables:Variables
variance:Varianza
variation_coefficient:Coeficiente de variaci�n
vector_space:Espacios vectoriales
vectorial_analysis:An�lisis vectorial
vectorial_field:Campo vectorial
vectorial_geometry:Geometr�a vectorial
vectors:Vectores
vegetal_tissue:Tejido vegetal
velocity:Velocidad
vibration:Vibraci�n 
virus:Virus
vocabulary:Vocabulario
voltage:Voltaje
volume:Volumen
ward_method:M�todo de Ward
weaver:
within_variance:Intra-varianza
written_english:Ingl�s escrito
zdomain: Otros dominios
zoology:Zoolog�a
