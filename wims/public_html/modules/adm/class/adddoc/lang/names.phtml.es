!set lang_exists=yes

!set name_intro=!nosubst Quiere que sea accesible en su clase \
el recurso p�blico <span class="tt wims_fname">$dir</span>

!set name_wksheet=Enlazar el documento con la hoja (opcional)

!set name_noone=Ninguna
!set name_propdoc=!nosubst Documento insertado n�mero $sdoc
!set name_proptool=!nosubst Outil ins�r� num�ro $stool
!set name_deletemsg=!nosubst �Quieres eliminar el documento \
  <span class="tt wims_fname">$title</span> de la lista de documentss\
  accesibles de la clase?
!set name_adddoc=Adici�n de un documento p�blico
!set name_addtool=Insertion d'un outil
!set name_confirmdoc=El documento p�blico se insert� correctamente en su clase.
!set name_confirmtool=L'outil a bien �t� ins�r� dans votre classe.
!set name_savemodif=Cambios guardados
!set name_confirmmksheet=!nosubst La hoja que contiene todos los ejercicios del documento se ha creado correctamente con el n�mero $autosheet
!set name_autosheet=N�mero de la hoja de ejercicios creada
!set name_nogenerated=No creada
!set name_generate=Crear
!set name_generate1=la hoja de ejercicios del documento
