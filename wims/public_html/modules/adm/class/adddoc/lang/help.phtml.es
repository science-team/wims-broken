!if $wims_read_parm!=$empty
 !goto $wims_read_parm
!endif

:linksh
<h2 id="general">
Vincular un documento a una hoja de ejercicios
</h2>
<p>
Puedes optar por enlazar el documento a una hoja de ejercicios
concreta que contiene las mismas series de ejercicios que los que hay
en el documento. Esto permite registrar el trabajo
realizado por los estudiantes cuando hacen clic en los enlaces de los ejercicios del documento.
</p><p>
Puedes crear la hoja de ejercicios tu mismo o utilizar los enlaces autom�ticos de creaci�n.
</p>
!exit

:autogenerate
<h2 id="statut">
Creaci�n autom�tica de la hoja de ejercicios de un documento
</h2>
<p>
Esta funcionalidad permite la creaci�n de una nueva hoja de ejercicios
que contiene la lista de todos los ejercicios del documento.
</p><p>
Se a�ade autom�ticamente a tu clase; la conexi�n entre
este documento y la hoja se instala autom�ticamente.
</p><p>
Despu�s puedes cambiar los par�metros de configuraci�n,
o posiblemente eliminar algunos ejercicios y modificar los pesos,
usando la interfaz habitual de gesti�n de las hojas de ejercicios
a partir de la p�gina de inicio de la clase.
</p><p>
Esta funcionalidad s�lo se puede utilizar una vez.
Si se quiere crear una copia de la hoja de nuevo,
se utilizar� la herramienta de duplicaci�n de la hoja de ejercicios.
</p>


!exit

:nohelp
No hay ayuda sobre este tema.
!exit

