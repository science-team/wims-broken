!set wims_module_log=error: $error

<span class="wims_warning">$wims_name_Error</span>.

!if not_supervisor=$error
 Lo sentimos pero la operaci�n de preparaci�n /modificaci�n de una hoja
 de trabajo est� reservada a los profesores registrados de la clase.
 !exit
!endif

!if bad_classpass=$error
 Lo sentimos pero no ha introducido la contrase�a correcta de la clase.
 No est� autorizado a cambiar la informaci�n sobre los participantes.
 !exit
!endif

!if bad_user=$error
 Error de llamada: el participante <span class="tt wims_login">$checkuser</span> no existe.
 !exit
!endif

!if badimgformat=$error
 Le fichier que vous avez envoy� n'est pas une image.
 !exit
!endif

!if filetoobig=$error
 Le fichier que vous avez envoy� d�passe la capacit� maximale autoris� apr�s conversion de l'image. Veuillez envoyer une image moins volumineuse.
 !exit
!endif

!if quota_file=$error
 La capacit� maximale de stockage de la classe est atteinte. Il n'est pas possible de sauvegarder votre image.
 !exit
!endif

!msg $error

