type=question
textarea="instruction data1 data2 data3 data4 data5 data6 data7 data8 data9 data10"
asis="data1 data2 data3 data4 data5 data6 data7 data8 data9 data10"
:Diverses preguntes consecutives, totes del tipus QCM

<p>Aquest exercici presenta preguntes d'opci� m�ltiple.</p>
<p>El nombre de preguntes est� limitat a 10. Es pot triar el nombre de preguntes
que es presentar� a cada exercici. Aquestes preguntes es realitzaran de forma aleat�ria o no.</p>
<p>Autor del model : Bernadette Perrin-Riou <bpr@math.u-psud.fr></p>

:%%%%%%%%%%%%%%%%%      ATTENTION      %%%%%%%%%%%%%%%%%%%%

Enlevez l'en-t�te ci-dessus si vous d�truisez les balises pour le mod�le !
(Ce sont les lignes qui commencent par un ':'.)
Sinon l'exercice risque de ne pas pouvoir repasser sous Createxo.

:%%%%%%%% Param�tres d'exemples � red�finir %%%%%%%%%%%%%%%%%

:\title{Opci� m�ltiple avan�ada}
:\author{Bernadette, Perrin-Riou}
:\email{bpr@math.u-psud.fr}
:\credits{Image de Pfly [CC BY-SA 2.5], via Wikimedia Commons}
:Instruccions Globals
Escriviu aqu� una instrucci� comuna a totes les preguntes de l'exercici
$embraced_randitem
\text{instruction=Respondre cada pregunta de la primera s�rie, validar i respondre les preguntes de la segona s�rie.}

:Ordre aleatori
Trieu "s�" per activar l'aleatorietat de les preguntes.
Trieu "no" per presentar-les en l'ordre en qu� es van entrar.
\text{alea=item(1,s�,no)}

:Nombre m�xim de preguntes per etapa
WIMS dividir� autom�ticament el vostre exercici en etapes en funci� del nombre total de preguntes establertes i del nombre de preguntes per etapa.
\text{N=3}

:Nombre m�xim d'etapes
WIMS dividir� autom�ticament el vostre exercici en etapes en funci� del nombre total de preguntes establertes i del nombre de preguntes per etapa.
\text{MAX=2}

:Text al costat de la resposta donada un cop l'alumne ha respost.
Aquestes paraules intervenen una vegada que les preguntes han estat respostes.
�s recomanable provar l'exercici de demostraci� amb
respostes correctes i incorrectes per entendre on intervenen aquestes paraules.
\text{qcm_prompt1=La vostra resposta:}

:Text per al feedback per a "La resposta correcta"

\text{qcm_prompt2=La(es) resposta(es) correcta(es) era(en):}

:Text per al feedback per a "La resposta correcta"

\text{good_answer_text=Resposta correcta!}

:Text per al feedback per a "Resposta incorrecta"

\text{bad_answer_text=Resposta incorrecta!}

:Text per al feedback per a "Resposta incompleta"

\text{incomplete_answer_text=Resposta incompleta...}

:Estil CSS per a les preguntes

\text{style_question=background-color: #F2F9FC;}

:Estil CSS per a les  respostes

\text{style_reponse=background-color: #FFCC99;}

:Percentatge d'�xit necessari per passar a la seg�ent etapa (si el nombre m�xim d'etapes > 1)

\text{percent=0}

:S'accepta text aleatori posat entre claus
$embraced_randitem
\text{accolade=item(2, s�, no)}

:Mostrar les respostes correctes
Si poseu s�, les respostes correctes es mostraran una vegada l'alumne respongui les preguntes.
\text{answer_given=item(1, s�, no)}

:Pregunta
<p>Introdu�u aqu� la pregunta que voleu fer, d'acord amb el seg�ent format:</p>
<ol>
<li>La primera l�nia cont� l'enunciat de la pregunta.</li>
<li> La segona l�nia representa un feedback, que es mostrar� despr�s de la validaci� de les respostes
 (es pot deixar en blanc).</li>
<li>Introdu�u a la tercera l�nia la llista dels n�meros de les respostes correctes, separats per comes. (Els n�meros representen l'ordre en qu� s'escriuen les propostes a continuaci�)</li>
<li>Cada l�nia seg�ent representa les diferents propostes de resposta.</li></ol>
<div style="border-left:2px solid orange;width:45em;background-color:white;padding:1em .5em;">Exemple: <br/>
<pre>Enunciat de la Pregunta 1
Explicaci� (feedback) que s'ha de mostrar en cas de resposta incorrecta a la pregunta
N�meros que indiquen el lloc de les respostes correctes
Proposta 1
Proposta 2
Proposta 3</pre></div>
<p class="oef_indbad"><strong>Atenci� :</strong>aqu� estan prohibits els punt i coma.</p>
<hr/>
<p><em>Opcional:</em> podeu afegir opcionalment una primera l�nia que contindr� variables per inserir un t�tol, una imatge, un so. (nb: ha d'estar en una classe o m�dul per transferir aquests fitxers). En aquest cas, ser� la 2a l�nia (i no la 1a) que representi l'enunciat, la 3a l�nia el feedback, etc.</p>
Les possibles variables s�n:
<ul>
  <li><code>Qtitle</code> : mostra un t�tol al principi de la pregunta. (per exemple, per indicar el tema)</li>
  <li><code>Qimage</code> : si s'especifica un fitxer d'imatge, aquesta es mostrar� al principi de la pregunta (nom�s funciona en un m�dul o classe).</li>
  <li><code>Qaudio</code> : si s'indica un fitxer de so, es mostrar� al principi de la pregunta (nom�s funciona en un m�dul o una classe. Bugs coneguts amb Safari).</li>
  </ul>
<div style="border-left:2px solid orange;width:45em;background-color:white;padding:1em .5em;">Exemple : <br/>
<pre>Qtitle="T�tol de la pregunta 2" Qimage="image.jpg" Qaudio="son.mp3"
Enunciat de la pregunta 2
Explicaci� (feedback) que s'ha de mostrar en cas de resposta incorrecta a la pregunta 2
N�mero(s) que indiquin el lloc de les respostes correctes
Proposta 1
Proposta 2
Proposta 3</pre></div>
\text{data1=asis(En quelle ann�e eut lieu la bataille de Marignan ?
Marignan fut la premi�re victoire du jeune roi Fran�ois Ier, la premi�re ann�e de son r�gne.
1
1515
1414
1313
1616)}

:Pregunta 2

\text{data2=asis(En 2014, quelle est la ville la plus peupl�e au monde ?
C'est Tokyo, la capitale du Japon (37,7 millions d'habitants), loin devant les autres (qui ont entre 20 et 24 millions d'habitants). <p class="right">Source <a href="http://www.populationdata.net/index2.php?option=palmares&rid=4&nom=grandes-villes-du-monde" target="_blank">populationdata.net</a></p>
7
Beijing
Karachi
Manille
Mexico
New York
Shangha�
Tokyo)}

:Pregunta 3

\text{data3=asis(Quel est le nom de ce fleuve : <img src="http://upload.wikimedia.org/wikipedia/commons/e/e4/Mekong_River_watershed.png"/>
Environ 70 millions d'habitants vivent directement dans le bassin versant du M�kong.
1
Le M�kong
Le Yangzi Jiang
La Volga
Le Danube)}

:Pregunta 4

\text{data4=asis(Qtitle="En Peinture"
Parmi ces diff�rents peintres, lesquels font partie du mouvement impressionniste ?
Ingres est associ� au courant Romantique, et Rapha�l � la Renaissance.
1,3
Edgar Degas
Dominique Ingres
Claude Monet
Rapha�l)}

:Pregunta 5

\text{data5=asis(Qtitle=ANIMAUX
Quel est le cri du chameau ?
Le brairement est le cri de l'�ne, le b�lement : celui des moutons et des ch�vres.
1
Le blat�rement
Le brairement
Le b�lement)}

:Pregunta 6

\text{data6=asis(Qtitle=Chimie
Quelle est la formule chimique de la testost�rone ?
C<sub>8</sub>H<sub>10</sub>N<sub>4</sub>O<sub>2</sub> correspond � la caf�ine, C<sub>3</sub>H<sub>5</sub>N<sub>3</sub>O<sub>9</sub> � la nitroglyc�rine, et C<sub>17</sub>H<sub>19</sub>NO<sub>3</sub> la morphine
4
C<sub>8</sub>H<sub>10</sub>N<sub>4</sub>O<sub>2</sub>
C<sub>3</sub>H<sub>5</sub>N<sub>3</sub>O<sub>9</sub>
C<sub>17</sub>H<sub>19</sub>NO<sub>3</sub>
C<sub>19</sub>H<sub>28</sub>O<sub>2</sub>)}

:Pregunta 7

\text{data7=asis(� partir de quel jour le beaujolais nouveau est-il disponible en vente ?
Le beaujolais nouveau est un vin de primeur, c'est-�-dire un vin de l'ann�e m�me, dont la commercialisation est autoris�e imm�diatement apr�s la fin de la vinification.
3
le premier jeudi de Novembre
le deuxi�me jeudi de Novembre
le troisi�me jeudi de Novembre
le quatri�me jeudi de Novembre)}

:Pregunta 8

\text{data8=asis(Dans le film <i>"Rain Man"</i>, quel acteur a un fr�re autiste surdou� ?
<i>Rain Man</i> est un film am�ricain r�alis� par Barry Levinson, sorti en 1989 en France.
2
Bruce Willis
Tom Cruise
Woody Allen)}

:Pregunta 9

\text{data9=asis(Le ski nautique est une discipline olympique depuis 1976.
L'apparition du ski nautique aux jeux olympiques ne date que de 2004. Toutefois, il avait �t� sport de d�monstration en 1972.
2
Vrai
Faux)}

:Pregunta 10

\text{data10=asis(Qtitle=Musique
Quel est le premier ballet �crit par Tcha�kovski ?
Le Lac des cygnes a �t� �crit en 1875 et Casse-noisette en 1891.
1
Le Lac des cygnes
Casse-noisette)}

:Tipus de respostes
El tipus "checkbox" (botons quadrats) permet diverses respostes per pregunta. <br/>
Mentre que el tipus "radio" (botons rodons) nom�s permet una resposta per pregunta.
\text{format=item(1, checkbox, radio)}

:
\language{ca}
\computeanswer{no}
\format{html}

\text{paste=yes}

\text{option=}

:%%%%%%%%%%%%%% Rien � modifier jusqu'� l'�nonc� %%%%%%%%%%%%%%%%

\text{data_q=\data1!= ? 1:}
\text{data_q=\data2!= ? wims(append item 2 to \data_q)}
\text{data_q=\data3!= ? wims(append item 3 to \data_q)}
\text{data_q=\data4!= ? wims(append item 4 to \data_q)}
\text{data_q=\data5!= ? wims(append item 5 to \data_q)}
\text{data_q=\data6!= ? wims(append item 6 to \data_q)}
\text{data_q=\data7!= ? wims(append item 7 to \data_q)}
\text{data_q=\data8!= ? wims(append item 8 to \data_q)}
\text{data_q=\data9!= ? wims(append item 9 to \data_q)}
\text{data_q=\data10!= ? wims(append item 10 to \data_q)}

\text{instruction=\accolade issametext s� ? wims(embraced randitem \instruction):\instruction}

\integer{cnt_question=items(\data_q)}

\text{nopaste=\paste issametext no ? slib(utilities/nopaste )}

\text{style = <style type="text/css">
 .enonce{margin-bottom:0}
 .qcm_prompt2{margin-top:.5em}
 img{vertical-align:middle;}
 .q_num, .panel .wims_title{
 font-size:150%;font-family:Impact, Charcoal, sans-serif;
 color:#777;
 }
 .feedback{border:1px dashed grey;padding:0.5em;margin:0;}
 .reponse {
   margin: 0 1em;
   padding: .5em;
   border-radius:5px;
   \style_reponse
 }
 .panel{
    padding:.5em 1em .5em 1em;
    margin:.5em 0;
    border:1px solid #d8d8d8;
    border-radius:5px;
    background-color: #f2f2f2;
 }
 .panel.callout{
    border-color:#c5e5f3;
    border-width:2px;
    \style_question
 }
 .callout .q_num, .callout .wims_title{color:black;}

 .panel .wims_title, .panel .wimscenter{margin-top:-1.2em;}
 ol li{margin-bottom: .5em;list-style: upper-alpha;}
 input[type='checkbox'] { font-size:150%; }
 .strike{text-decoration:line-through;}
 .oef_indpartial{color:navy;}
 #answeranalysis{display:none;}
 </style>
}

\integer{N = min(\cnt_question,\N)}
\integer{MAX=min(\N*\MAX,\cnt_question)}
\text{battage=\alea issametext s� ? shuffle(\data_q,,) :\data_q}
\text{battage=wims(nonempty items \battage)}

\text{option=\option noanalyzeprint}
\matrix{question=}
\matrix{explication=xxx}
\matrix{rep=}
\text{CNT_choix= }
\matrix{CHOIX=}
\text{PRELIMINAIRE=}

\for{i= 1 to \MAX}{
 \matrix{QUEST=\battage[\i]=1? \data1}
 \matrix{QUEST=\battage[\i]=2? \data2}
 \matrix{QUEST=\battage[\i]=3? \data3}
 \matrix{QUEST=\battage[\i]=4? \data4}
 \matrix{QUEST=\battage[\i]=5? \data5}
 \matrix{QUEST=\battage[\i]=6? \data6}
 \matrix{QUEST=\battage[\i]=7? \data7}
 \matrix{QUEST=\battage[\i]=8? \data8}
 \matrix{QUEST=\battage[\i]=9? \data9}
 \matrix{QUEST=\battage[\i]=10? \data10}

 \text{preliminaire_test=\QUEST}
 \text{preliminaire_test=row(1,\preliminaire_test)}
 \text{inst_audio=wims(getopt Qaudio in \preliminaire_test)}
 \text{inst_image=wims(getopt Qimage in \preliminaire_test)}
 \text{inst_title=wims(getopt Qtitle in \preliminaire_test)}

 \text{rab_inst=}
 \text{rab_inst=\inst_title notsametext ?\rab_inst <h2 class="wims_title">\inst_title</h2>}
 \text{rab_inst=\inst_image notsametext ?\rab_inst <div class="wimscenter"><img src="\imagedir/\inst_image" alt="" /></div>}
 \if{\inst_audio notsametext }{
     \text{rab_inst1= . isin \inst_audio ?
       <audio controls>
        <source src="\imagedir/\inst_audio" type="audio/mpeg">
        D�sol�, votre navigateur est incompatible avec la lecture de fichiers audio.
       </audio>}
     \text{rab_inst=\rab_inst <div class="wimscenter audio">\rab_inst1[1;1]</div>}
 }

 \integer{test_inst=\inst_audio\inst_image\inst_title notsametext ? 1 : 0}
 \text{preliminaire=\test_inst=1 ? \rab_inst:&nbsp;}
 \matrix{QUEST=\test_inst=1 ? \QUEST[2..-1;]}
 \matrix{QUEST=\accolade issametext oui ?wims(embraced randitem \QUEST):\QUEST}
 \matrix{question = \question
\QUEST[1;]}
 \matrix{PRELIMINAIRE=\PRELIMINAIRE
\preliminaire}

 \text{expl=\QUEST[2;]}
 \text{ligne=wims(upper \expl)}
 \text{ligne=wims(text select ABCDEFGHIJKLMNOPQRSTUVWXYZ in \ligne)}
 \if{\ligne issametext and \expl notsametext }{
  \matrix{explication = \explication;}
  \integer{debut = 2}
 }
 \if{\ligne issametext and \expl issametext }{
  \matrix{explication = \explication;}
  \integer{debut = 3}
  }
  \if{\ligne notsametext}{
     \matrix{explication = \explication;\expl}
  \integer{debut = 3}
 }
  \integer{cnt_choix=rows(\QUEST)-\debut}
  \text{CNT_choix=\CNT_choix,\cnt_choix}
   \text{Choix=}
   \text{mix=shuffle(\cnt_choix)}
   \for{ j=\debut+1 to \cnt_choix + \debut+1}{
     \text{choix= \QUEST[\j;]}
     \text{choix=wims(replace internal , by  &#44; in \choix)}
     \matrix{Choix = \Choix, \choix[1;]}
   }
   \text{Choix=wims(nonempty items \Choix)}
   \text{Choix= \Choix[\mix]}
   \matrix{CHOIX=\CHOIX
     \Choix}
   \text{H = wims(nospace \QUEST[\debut;])}
   \text{cnt_c=items(\H)}
   \text{Rep = }
   \for{ k = 1 to \cnt_c}{
     \text{Rep = \Rep, position(\H[\k],\mix)}
   }
   \text{Rep = wims(sort items wims(nonempty items \Rep))}
   \matrix{rep = \rep
    \Rep}
}

\text{CNT_choix=wims(nonempty items \CNT_choix)}

\text{U = pari(divrem(\MAX,\N)~)}
\integer{cnt_step = \U[1] + 1}
\matrix{STEPS = }
\matrix{CNT = }
\text{CONDSTEP=}
\for{ u = 1 to \cnt_step -1}{
   \matrix{STEPS =\STEPS
wims(makelist r x for x = \N*\u -\N+1 to \N*\u)}
   \matrix{CNT =\CNT
wims(makelist x for x = \N*\u -\N+1 to \N*\u)}
   \text{condstep= wims(values \u+1 for x = (\u-1)*\N +1 to \u*\N)}
   \text{CONDSTEP= wims(append item \condstep to \CONDSTEP)}
}
 \matrix{STEPS = \STEPS
 wims(makelist r x for x = \N*\cnt_step-\N+1 to \MAX)
}
\matrix{CNT = \CNT
 wims(makelist x for x = \N*\cnt_step-\N+1 to \MAX)
}
\text{CONDSTEP=\CONDSTEP, wims(values \cnt_step+1 for x = \N*\cnt_step-\N+1 to \MAX)}

\text{nstep=\STEPS[1;]}
\text{TEST=}
\text{explication=\explication[2..-1;]}

text{testexp=wims(rows2lines \explication)}
text{testexp=wims(lines2items \testexp)}
text{testexp=wims(items2words \testexp)}
text{testexp=wims(nospace \testexp)}
\nextstep{\nstep}
\text{REP=}
\text{etape=wims(values x * \N for x = 1 to \cnt_step+1)}
\text{CONDITION = wims(makelist x for x = 1 to 2*\MAX)}
\text{CONDITION =wims(items2words \CONDITION)}
\conditions{\CONDITION}
\integer{cnt_juste=0}
\real{v = 10}

\statement{\nopaste
  \style
  <div class="instructions">\instruction</div>

  \for{h=1 to \etape[\step]}{

   \if{\question[\h;] notsametext }{
     \if{\h <= \etape[\step] - \N}
       {<div class="panel">}
       {<div class="panel callout">}
    }

   \if{(\h <= \etape[\step] - \N or r \h isitemof \nstep) and \question[\h;] notsametext }{
     <div class="enonce">
       \if{\cnt_step > 1 and \MAX > 1}{<span class="q_num">\h :</span>}
       <span class="preliminaire">\PRELIMINAIRE[\h;]</span>
       <span class="question">\question[\h;]</span>
     </div>
   }

   \if{\h <= \etape[\step] - \N and \question[\h;] notsametext}{
    <div class="reponse">
      <span class="qcm_prompt1">\qcm_prompt1</span>
       \for{ a in \REP[\h;]}{
         \if{ \a isitemof \CHOIX[\h;\rep[\h;]]}{
           \if{\TEST[\h;2]>0 and \TEST[\h;3]=0}{
           <span class="oef_indpartial">\a</span>}
           {<span class="oef_indgood">\a</span>}
           }
         {
         <span class="oef_indbad strike">\a</span>} - }
         \if{\TEST[\h;2]>0 and \TEST[\h;3]=0}
           {<span class="oef_indpartial">\incomplete_answer_text</span>}
         \if{\TEST[\h;3]>0}
           {<span class="oef_indbad">\bad_answer_text</span>}
         \if{\TEST[\h;3]=0 and \TEST[\h;2]=0}
           {<span class="oef_indgood">\good_answer_text</span>}
         \if{\answer_given issametext s�}{
           <p class="qcm_prompt2"> \qcm_prompt2</p>
           <ul class="oef_indgood">
             \for{s=1 to \CNT_choix[\h]}{
               \if{\s isitemof \rep[\h;]}{ <li>\CHOIX[\h;\s]</li>}
             }
           </ul>
         }
         \if{\explication[\h;] notsametext }{<div class="feedback">\explication[\h;]</div>}
     </div>
   }
   { \if{ r \h isitemof \nstep}{
      <div class="question">
        <ol>
          \for{s=1 to \CNT_choix[\h]}{ <li>\embed{reply \h , \s}</li> }
        </ol>
      </div>
      }
   }

   \if{\question[\h;] notsametext }{</div>}
  }
 }
\answer{}{\REP1;\CHOIX[1;]}{type=\format}{option=\option}
\answer{}{\REP2;\CHOIX[2;]}{type=\format}{option=\option}
\answer{}{\REP3;\CHOIX[3;]}{type=\format}{option=\option}
\answer{}{\REP4;\CHOIX[4;]}{type=\format}{option=\option}
\answer{}{\REP5;\CHOIX[5;]}{type=\format}{option=\option}
\answer{}{\REP6;\CHOIX[6;]}{type=\format}{option=\option}
\answer{}{\REP7;\CHOIX[7;]}{type=\format}{option=\option}
\answer{}{\REP8;\CHOIX[8;]}{type=\format}{option=\option}
\answer{}{\REP9;\CHOIX[9;]}{type=\format}{option=\option}
\answer{}{\REP10;\CHOIX[10;]}{type=\format}{option=\option}

\matrix{REP = \REP1
\REP2
\REP3
\REP4
\REP5
\REP6
\REP7
\REP8
\REP9
\REP10}
\if{\format=radio}{
 \text{REP=wims(replace internal , by &#44; in \REP)}
}
\matrix{explication2 = \explication2}

\for{u = 1 to \N}{
   \text{H = \CNT[\step-1;\u]}
   \text{test1 = wims(listuniq \REP[\H;],\CHOIX[\H;\rep[\H;]])}
   \integer{test1 = items(\test1)-items(\CHOIX[\H;\rep[\H;]])}
   \text{test2 = wims(listcomplement \REP[\H;] in \CHOIX[\H;\rep[\H;]])}
   \text{test3 = wims(listcomplement \CHOIX[\H;\rep[\H;]] in \REP[\H;])}
  %%% \integer{test4=items(\REP[\H;]) - items(\CHOIX[\H;])}
   \text{test_cnt=\test1, items(\test2),items(\test3)}
   \integer{cnt_juste= \test_cnt[1]+\test_cnt[2]+\test_cnt[3] =0 ? \cnt_juste + 1}
   \matrix{TEST=\TEST
   \test_cnt}
}

test1 = 0 rep < juste
test2 nombre de r�ponses dites justes et en fait fausses
test3 nombre de r�ponses dites fausses et en fait justes

totalement justes : test1=0, test2=0 test3=0
partiellement justes :

\real{v=\cnt_juste/\CNT[\step-1;\N]}

\text{nstep = \v >= \percent ? \STEPS[\step;]:}

\condition{Question 1 : \REP1}{\TEST[1;3]=0}{option=hide}
\condition{Question 1 : \REP1}{\TEST[1;1]=0 and \TEST[1;2]=0 and \TEST[1;3]=0}{option=hide}
\condition{Question 2 : \REP2}{\TEST[2;3]=0 and \step >=\CONDSTEP[2]}{option=hide}
\condition{Question 2 : \REP2}{\TEST[2;1]=0 and \TEST[2;2]=0 and \TEST[2;3]=0 and \step >=\CONDSTEP[2]}{option=hide}
\condition{Question 3 : \REP3}{\TEST[3;3]=0 and \step >=\CONDSTEP[3]}{option=hide}
\condition{Question 3 : \REP3}{\TEST[3;1]=0 and \TEST[3;2]=0 and \TEST[3;3]=0 and \step >=\CONDSTEP[3]}{option=hide}
\condition{Question 4 : \REP4}{\TEST[4;3]=0 and \step >=\CONDSTEP[4]}{option=hide}
\condition{Question 4 : \REP4}{\TEST[4;1]=0 and \TEST[4;2]=0 and \TEST[4;3]=0 and \step >=\CONDSTEP[4]}{option=hide}
\condition{Question 5 : \REP5}{\TEST[5;3]=0 and \step >=\CONDSTEP[5]}{option=hide}
\condition{Question 5 : \REP5}{\TEST[5;1]=0 and \TEST[5;2]=0 and \TEST[5;3]=0 and \step >=\CONDSTEP[5]}{option=hide}
\condition{Question 6 : \REP6}{\TEST[6;3]=0 and \step >=\CONDSTEP[6]}{option=hide}
\condition{Question 6 : \REP6}{\TEST[6;1]=0 and \TEST[6;2]=0 and \TEST[6;3]=0 and \step >=\CONDSTEP[6]}{option=hide}
\condition{Question 7 : \REP7}{\TEST[7;3]=0 and \step >=\CONDSTEP[7]}{option=hide}
\condition{Question 7 : \REP7}{\TEST[7;1]=0 and \TEST[7;2]=0 and \TEST[7;3]=0 and \step >=\CONDSTEP[7]}{option=hide}
\condition{Question 8 : \REP8}{\TEST[8;3]=0 and \step >=\CONDSTEP[8]}{option=hide}
\condition{Question 8 : \REP8}{\TEST[8;1]=0 and \TEST[8;2]=0 and \TEST[8;3]=0 and \step >=\CONDSTEP[8]}{option=hide}
\condition{Question 9 : \REP9}{\TEST[9;3]=0 and \step >=\CONDSTEP[9]}{option=hide}
\condition{Question 9 : \REP9}{\TEST[9;1]=0 and \TEST[9;2]=0 and \TEST[9;3]=0 and \step >=\CONDSTEP[9]}{option=hide}
\condition{Question 10 : \REP10}{\TEST[10;3]=0 and \step >=\CONDSTEP[10]}{option=hide}
\condition{Question 10 : \REP10}{\TEST[10;1]=0 and \TEST[10;2]=0 and \TEST[10;3]=0 and \step >=\CONDSTEP[10]}{option=hide}



\text{test=wims(rows2lines \explication)}
\text{test=wims(lines2items \test)}
\text{test=wims(items2words \test)}
\text{test=wims(nospace \test)}
feedback{1=1}{\explication
   \if{\test notsametext}{
     <div class="reponse"><ol>
      \for{w = 1 to \MAX}{
        \if{\explication[\w;] notsametext }
         {<li style="list-style:decimal;" value="\w">\explication[\w;] </li>}
     }
     </ol>
    </div>
  }
}
{<ol>
  \for{ t = 1 to \N}{
    \if{\CNT[\step;\t] != }{
  <li style="list-style:decimal;" value="\CNT[\step;\t]"> <b>\question[\N*(\step-1) + \t;]</b>
  <div class="question"><ol>
   \for{s=1 to \CNT_choix[\N*(\step-1) + \t]}{
   <li>\embed{\STEPS[\step;\t] , \s }</li>
    }
   </ol>
   </div>
   </li>}
 }
</ol>
}