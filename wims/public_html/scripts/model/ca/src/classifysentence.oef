type=classify
textarea="data"

:Classificar totes les paraules d'una frase escrita d'acord amb un atribut (per exemple, la seva naturalesa).
Aquest exercici �s diferent d'altres exercicis de classificaci� en la forma en qu� les dades s�n
entrades, les dades s�n una frase i no una llista de paraules.
<p>
Autor del model: Bernadette Perrin-Riou <bpr@math.u-psud.fr>
</p>
:%%%%%%%%%%%%%%%%%      ATTENTION      %%%%%%%%%%%%%%%%%%%%

Enlevez l'en-t�te ci-dessus si vous d�truisez les balises pour le mod�le !
(Ce sont les lignes qui commencent par un ':'.)
Sinon l'exercice risque de ne pas pouvoir repasser sous Createxo.

:%%%%%%%% Param�tres d'exemples � red�finir %%%%%%%%%%%%%%%%%
:\title{Classificar les paraules d'una frase}
:\author{Bernadette, Perrin-Riou}
:\email{}
:\credits{}
:Enunciat
\text{instruction= Classer les mots selon leur nature ou des propri�t�s}

:Nom del fitxer
Ompliu-ho nom�s si voleu transferir el codi d'aquest exercici a un compte de Modtool
i utilitzar un fitxer de dades (en un m�dul OEF)
del tipus
<pre>
 V,N,Art,Adj,Prep
 Verbe,Nom,Article,Adjectif,Pr�position
 &#58;Le|Art,petit|Adj,chat|N,boit|V,du|Art,lait.|N
 &#58;Le|Art,chat|N,boit|V,du|Art,lait|N,dans|Prep,un|Art,bol.|N
</pre>
En aquest cas, les dades principals no es tindran en compte.
En cas contrari, ompliu les dades principals.

\text{file=}

:Naturalesa dels objectes
\text{nom_nature=Verbe,Nom,Article,Adjectif,Pr�position}

:Naturalesa dels objectes (codi)
Aquest codi s'utilitzar� a les dades principals.
\text{Nature=V,N,Art,Adj,Prep}

:Dades principals.
Una frase per l�nia: les paraules (o grups de paraules) de la frase estan separades per comes. Despr�s de cada paraula,
separada de la paraula per la barra vertical <span class="tt">|</span>, est� escrita la naturalesa o l'atribut de la paraula tal
que estava escrivint al camp <span class="tt">Naturalesa de les paraules (codi)</span>. Si la paraula (o grup de paraules)
no va seguida d'una barra vertical
i un atribut, no es proposa a la llista de paraules a classificar,
encara que estigui escrita en la frase introduct�ria.

\text{data=Le|Art,petit|Adj,chat|N,boit|V,du|Art,lait|N
Le|Art,chat|N,boit|V,du|Art,lait|N,dans|Prep,un|Art,bol|N
Le,chat|N,boit|V,du,lait|N,dans|Prep,un,bol|N}

:S'accepta text aleatori posat entre claus
$embraced_randitem
\text{accolade=item(1,1 s�,
2 no)}

:%%%%%%%%%%%%%% Rien � modifier avant l'�nonc� %%%%%%%%%%%%%%%%
\css{<style type="text/css">
   .question {background-color: #FFFFCC;margin: 2% 2%;padding: 1%;}
   .reponse {background-color: #FFCC99;color: black;margin: 2% 2%;padding: 0%;}
   .oefstatement{}
   ol li {list-style: upper-alpha; }
 </style>}
 ##s'il y a un nom de fichier dans file, utilise le fichier, sinon utilise data
\if{\file notsametext}{
  \text{data=wims(randrecord \file)}
  \text{Nature=wims(record 0 of \file)}
  \text{nom_nature=row(2,\Nature)}
  \text{Nature=row(1,\Nature)}
  }{
  \text{data=randomrow(\data)}
  }
\text{accolade=wims(word 1 of \accolade)}

\text{data=\accolade=1 ? wims(embraced randitem \data)}
\integer{cnt=items(\data)}
\integer{Nat_cnt=items(\Nature)}
\text{phrase=}
\text{nature=}
\text{tableau=wims(makelist videxxx for x = 1 to \Nat_cnt)}
\text{tableau=wims(items2lines \tableau)}
\for{u=1 to \cnt}{
  \text{w=\data[\u]}
  \text{w=wims(replace internal | by , in \w)}
  \text{phrase=\phrase \w[1]}
  \for{h = 1 to \Nat_cnt}{
    \if{\w[2] issametext \Nature[\h]}{
       \text{ligne=row(\h,\tableau)}
       \text{ligne=videxxx isitemof \ligne ? \w[1]:\ligne,\w[1]}
       \text{tableau=wims(replace line number \h by \ligne in \tableau)}
       \text{nature=wims(append item \w[2] to \nature)}
    }
   }
}
\matrix{tableau=\tableau}
\text{nature1=wims(listuniq \nature)}
\text{nature=}
\for{ a in \Nature}{
   \text{nature=\a isin \nature1 ? wims(append item \a to \nature)}
}
\text{question=}
\text{nom_question=}
\text{Step=}
\for{a in \nature}{
   \text{qu=position(\a,\Nature)}
   \text{Step=wims(append item r \qu to \Step)}
   \text{question=wims(append item \qu to \question)}
   \text{nom_question=wims(append item \nom_nature[\qu] to \nom_question)}
}
\integer{question_cnt=items(\question)}
\steps{\Step}

\statement{\instruction
<div class="question">\phrase.</div>
<table class="wimsnoborder">
\for{ s = 1 to \question_cnt}{
  <tr><th>\nom_question[\s]</th><td>\embed{\Step[\s],80 x 30 x \cnt}</td></tr>
}
</table>
}
\answer{\nom_question[1]}{\tableau[1;]}{type=dragfill}{option=noorder}
\answer{\nom_question[2]}{\tableau[2;]}{type=dragfill}{option=noorder}
\answer{\nom_question[3]}{\tableau[3;]}{type=dragfill}{option=noorder}
\answer{\nom_question[4]}{\tableau[4;]}{type=dragfill}{option=noorder}
\answer{\nom_question[5]}{\tableau[5;]}{type=dragfill}{option=noorder}
\answer{\nom_question[6]}{\tableau[6;]}{type=dragfill}{option=noorder}
\answer{\nom_question[7]}{\tableau[7;]}{type=dragfill}{option=noorder}
\answer{\nom_question[8]}{\tableau[8;]}{type=dragfill}{option=noorder}
\answer{\nom_question[9]}{\tableau[9;]}{type=dragfill}{option=noorder}
\answer{\nom_question[10]}{\tableau[10;]}{type=dragfill}{option=noorder}
