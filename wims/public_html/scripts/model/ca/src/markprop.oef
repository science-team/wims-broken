type=select
textarea="data explain"

:Seleccionar els objectes segons la seva categoria.

Es presenta una llista d'objectes i s'han de marcar aquells que tenen una propietat determinada
segons la sol�licitud de l'exercici. <br/>
Per construir un exercici amb aquest model, simplement doneu la llista d'objectes i les seves propietats. <br/>
Autor del model: Gang Xiao <qualite@wimsedu.info>

:%%%%%%%%%%%%%%%%%      ATTENTION      %%%%%%%%%%%%%%%%%%%%

Enlevez l'en-t�te ci-dessus si vous d�truisez les balises pour le mod�le !
(Ce sont les lignes qui commencent par un ':'.)
Sinon l'exercice risque de ne pas pouvoir repasser sous Createxo.

:%%%%%%%% Param�tres d'exemples � red�finir %%%%%%%%%%%%%%%%%

:\title{Seleccionar per categoria}
:\author{XIAO, Gang}
:\email{qualite@wimsedu.info}
:\credits{}

:Nombre d'objectes a marcar per exercici. Fins a 100.
Heu de tenir definits suficients objectes a les dades principals.
\integer{tot=12}

:El nombre m�nim de bons objectes a marcar. Almenys 1.
\integer{mingood=1}

:El nombre m�nim d'objectes incorrectes que no s'han de marcar. Almenys 1.
\integer{minbad=4}

:Llista de propietats. La propietat que es marcar� es tria a l'atzar.
\text{prop=fran�aise,italienne,allemande}

:Dades principals.
Escriviu (un per l�nia) els objectes a classificar, despr�s la seva categoria, separats per una coma. <p>
L'objecte pot ser una paraula, una frase (sense comes). �s millor definir abundants dades per a cada propietat.
\matrix{data=
Fabien Barthez,francaise
Gr�gory Coupet,francaise
Eric Abidal,francaise
William Gallas,francaise
Willy Sagnol,francaise
Lilian Thuram,francaise
Pascal Chimbonda,francaise
Micka�l Silvestre,francaise
Claude Mak�l�l�,francaise
Patrick Vieira,francaise
Florent Malouda,francaise
Zinedine Zidane,francaise
Sylvain Wiltord,francaise
Franck Rib�ry,francaise
Vikash Dhorasoo,francaise
Sidney Govou,francaise
Thierry Henry,francaise
David Tr�z�guet,francaise
Louis Saha,francaise

Gianluigi Buffon,italienne
Morgan De Sanctis,italienne
Angelo Peruzzi,italienne
Christian Abbiati,italienne
Marco Amelia,italienne
Flavio Roma,italienne
Gianluca Zambrotta,italienne
Alessandro Nesta,italienne
Fabio Cannavaro,italienne
Fabio Grosso,italienne
Cristian Zaccardo,italienne
Marco Materazzi,italienne
Andrea Barzagli,italienne
Massimo Oddo,italienne
Mauro Camoranesi,italienne
Andrea Pirlo,italienne
Gennaro Gattuso,italienne
Francesco Totti,italienne
Daniele de Rossi,italienne
Aimo Diana,italienne
Simone Barone,italienne
Manuele Blasi,italienne
Simone Perrotta,italienne
Alberto Gilardino,italienne
Luca Toni,italienne
Alessandro Del Piero,italienne
Vicenzo Iaquinta,italienne
Christian Vieri,italienne
Antonio Cassano,italienne
Cristiano Lucarelli,italienne
Filippo Inzaghi,italienne

Jens Lehmann,allemande
Oliver Kahn,allemande
Timo Hildebrand,allemande
Philipp Lahm,allemande
Arne Friedrich,allemande
Robert Huth,allemande
Jens Nowotny,allemande
Per Mertesacker,allemande
Christoph Metzelder,allemande
Marcell Jansen,allemande
Bernd Schneider,allemande
Sebastian Kehl,allemande
Torsten Frings,allemande
Michael Ballack,allemande
Tim Borowski,allemande
Bastian Schweinsteiger,allemande
David Odonkor,allemande
Thomas Hitzlsperger,allemande
Miroslav Klose,allemande
Lukas Podolski,allemande
Gerald Asamoah,allemande
Mike Hanke,allemande
Oliver Neuville,allemande
}

:Opcions.
Afegiu la paraula <span class="tt wims_code_words">split</span>  a la definici� si voleu donar una nota 
parcial per a respostes parcialment correctes. 
\text{option=split}

:Opcions de presentaci�.
Afegiu aqu� la paraula "liste" si voleu mostrar els objectes com una llista.
\text{presentation=}

:S'accepta text aleatori posat entre claus
$embraced_randitem
\text{accolade=item(1,1 s�,
2 no)}

:%%%%%%%%%%%%%% Rien � modifier avant l'�nonc� %%%%%%%%%%%%%%%%

\text{prop=randitem(\prop)}

\text{data=wims(nonempty rows \data)}
\text{good=wims(select \data where column 2 issametext \prop)}
\text{bad=wims(select \data where column 2 notsametext \prop)}
\text{good=shuffle(wims(nonempty item \good[;1]))}
\text{bad=shuffle(wims(nonempty item \bad[;1]))}
\integer{goodcnt=items(\good)}
\integer{badcnt=items(\bad)}
\integer{tot=min(\tot,\goodcnt+\badcnt)}
\integer{mingood=min(\mingood,\tot-1)}
\integer{minbad=min(\minbad,\tot-\mingood-1)}
\integer{pickgood=randint(min(\mingood,\goodcnt)..min(\tot-\minbad,\goodcnt))}
\integer{pickbad=min(\tot-\pickgood,\badcnt)}
\integer{tot=\pickgood+\pickbad}
\text{ind=wims(makelist 1 for x=1 to \pickgood),wims(makelist 0 for x=1 to \pickbad)}
\text{all=item(1..\pickgood,\good),item(1..\pickbad,\bad)}
\text{list=wims(values x for x=1 to \tot)}
\text{sh=shuffle(\list)}
\text{all=item(\sh,\all)}
\text{ind=item(\sh,\ind)}
\text{tomark=positionof(1,\ind)}

:%% Choix de la presentation : sous forme lineaire par defaut, ou sous forme de liste � puces.
\if{liste iswordof \presentation}
{
 \text{pre=<ul>}
 \text{separatorL=<li>}
 \text{separatorR=</li>}
 \text{post=</ul>}
}
{
 \text{pre=<p>}
 \text{separatorL=<span>}
 \text{separatorR=</span>}
 \text{post=</p>}
}

:%%%%%%%%%%%%% Maintenant l'�nonc� en code html. %%%%%%%%%%%%%%%%%%%%

:El text per explicar qu� fer.
Per evocar la propietat demanada, escriviu <span class="tt">\prop</span>.
$embraced_randitem
\text{explain=
Parmi les joueurs ci-dessous qui ont particip� � la coupe du monde de football 2006, s�lectionner ceux qui sont dans l'�quipe \prop.
}

:%% Calculs � ne pas modifier
\text{accolade=wims(word 1 of \accolade)}

\text{explain=\accolade=1 ?wims(embraced randitem \explain)}

::On n'a pas besoin de modifier l'�nonc� directement en g�n�ral.

\statement{
<p>\explain</p>
\pre
 \for{k=1 to \tot-1}{\separatorL \embed{r1,\k},\separatorR }
 \separatorL \embed{r1,\tot}.\separatorR
\post
}

:%%%%%%%%%%%%% Rien � modifier ci-apr�s. %%%%%%%%%%%%%%%%%5

\answer{Le marquage}{\tomark;\all}{type=mark}{option=\option}

