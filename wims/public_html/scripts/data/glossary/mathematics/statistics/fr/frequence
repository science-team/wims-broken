!!abstract,linked gloses,internal links,content,dynamic examples,...
!set gl_author=Euler, Acad�mie de Versailles
!set gl_keywords=descriptive_statistics
!set gl_title=Fr�quence
!set gl_level=H1 Cycle4
:
:
:
:
:
<div class="wims_defn"> <h4>D�finitions</h4>
Soit \(S\) une s�rie statistique � une variable et \(a\) une modalit� de \(S\).<br/>
La <strong>fr�quence</strong> associ�e � \(a\) est le quotient de l'effectif de cette modalit� par l'effectif total de la s�rie.<br/>
Dans le cas d'une s�rie \(S\) de type quantitatif dont les modalit�s sont regroup�es en classes, la <strong>fr�quence</strong>
d'une classe est le quotient de l'effectif de cette classe par l'effectif total
de la s�rie.
</div>
<div class="wims_rem"><h4>Remarques</h4>
<ul>
<li>La fr�quence d'une modalit� repr�sente la proportion d'individus dans la population pour lesquels le caract�re �tudi� a cette modalit� ; pour une s�rie de type quantitatif dont les valeurs sont regroup�es en classes, la fr�quence d'une classe repr�sente la proportion d'individus dans la population pour lesquels le caract�re �tudi� a une modalit� dans cette classe.</li>
<li>La fr�quence d'une modalit� ou d'une classe est un nombre compris entre 0 et 1 ; elle peut aussi �tre exprim�e en pourcentage de l'effectif total.</li>
<li>La somme des fr�quences de toutes les modalit�s ou de toutes les classes est �gale � 1 (ou �
<math xmlns='http://www.w3.org/1998/Math/MathML'>
 <mrow>
  <mn>100</mn>
  <mtext>%</mtext>
 </mrow>
</math>
 pour les fr�quences exprim�es en pourcentages).</li>
</ul>
</div>
