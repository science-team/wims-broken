!!abstract,linked gloses,internal links,content,dynamic examples,...
!set gl_author=Euler, Acad�mie de Versailles
!set gl_keywords=integral
!set gl_title=Primitive d'une fonction num�rique
!set gl_level=H6 S ES L
:
:
:
:<div class="wims_defn">
<h4>D�finition</h4>
Soit \(f\) une fonction d�finie sur un intervalle \(I\).<br/>
On appelle <span class="wims_emph">primitive</span> de \(f\) sur \(I\) toute fonction
\(F\) d�rivable sur \(I\)
telle que pour tout
\(x \in I\), \(F'(x)=f(x)\).
</div>
<div class="wims_thm"><h4>Th�or�me</h4>
Toute fonction \(f\) continue sur un intervalle \(I\) admet une primitive sur \(I\).</div>
<div class="wims_thm">
<div><h4>Th�or�me</h4>
Soit \(f\) une fonction continue sur un intervalle \(I\) et \(F\) une primitive de \(f\) sur \(I\).
<ul><li>Soit \(k\in \RR\).
La fonction \(G\) d�finie pour tout \(x \in I\)
par \(G(x)=F(x)+k\) est une primitive de \(f\) sur \(I\).</li>
<li>
Pour toute primitive \(G\) de \(f\) sur \(I\),
il existe un r�el \(k\) tel que, pour tout
\(x \in I \), \(G(x) =F(x) + k\).
</li></ul></div></div>
<div class="wims_thm"><h4>Th�or�me</h4>
Soit \(f\) une fonction continue sur un intervalle \(I\).<br/>
Soit \(x_0 \in I\) et \(y_0 \in \RR\).
<br/>
Il existe une unique primitive \(F\) de \(f\) sur \(I\) telle que
\(F(x_0)= y_0\).</div>
