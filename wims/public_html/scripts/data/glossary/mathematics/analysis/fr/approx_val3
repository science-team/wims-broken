!!abstract,linked gloses,internal links,content,dynamic examples,...
!set gl_author=Euler, Acad�mie de Versailles
!set gl_keywords=real_number
!set gl_title=Valeur approch�e par d�faut, valeur approch�e par exc�s (coll�ge)
!set gl_level=E6 Cycle3
:
:
:
:
<div class="wims_defn"><h4>D�finitions</h4>
Soit \(x\) et \(a\) deux nombres.
<ul>
<li>
Le nombre \(a\) est <strong>une valeur approch�e par d�faut</strong>
de \(x\) � l'unit� pr�s</strong>, ou � la pr�cision 1, lorsque
\(0\leqslant x-a \leqslant 1\)
c'est-�-dire lorsque \(a\leqslant x \leqslant a+1\).
<br/>
Le nombre \(a\) est <strong>une valeur approch�e par d�faut</strong>
de \(x\) au dixi�me pr�s</strong>, ou � la pr�cision 0,1, lorsque
\(0\leqslant x-a \leqslant 0,1\)
c'est-�-dire lorsque \(a\leqslant x \leqslant a+0,1\)
<br/>
Le nombre \(a\) est <strong>une valeur approch�e par d�faut</strong>
de \(x\) au centi�me pr�s</strong>, ou � la pr�cision 0,01, lorsque
\(0\leqslant x-a \leqslant 0,01\)
c'est-�-dire lorsque \(a\leqslant x \leqslant a+0,01\)
</li>
<li>
Le nombre \(a\) est <strong>une valeur approch�e par exc�s</strong>
de \(x\) � l'unit� pr�s</strong>, ou � la pr�cision 1, lorsque
\(0\leqslant a-x \leqslant 1)
c'est-�-dire lorsque \(a-1\leqslant x \leqslant a\)
<br/>
Le nombre \(a\) est <strong>une valeur approch�e par exc�s</strong>
de \(x\) au dixi�me pr�s</strong>, ou � la pr�cision 0,1, lorsque
\(0\leqslant a-x \leqslant 0,1\)
c'est-�-dire lorsque \(a-0,1\leqslant x \leqslant a\)
<br/>
Le nombre \(a\) est <strong>une valeur approch�e par exc�s</strong>
de \(x\) au centi�me pr�s</strong>, ou � la pr�cision 0,01, lorsque
\(0\leqslant a-x \leqslant 0,01\)
c'est-�-dire lorsque \(a-0,01\leqslant x \leqslant a\)
</ul>
</div>

<div class="wims_rem"><h4>Remarque</h4>
Soit \(p\) un nombre strictement positif.<br/>
<ul>
<li>
Le nombre \(a\) est une <strong>valeur approch�e par d�faut</strong> de \(x\)
� \(p\) pr�s, ou � la pr�cision \(p\), lorsque \(0\leqslant x-a \leqslant p\)
c'est-�-dire lorsque \(a\leqslant x\leqslant a + p \) ;
</li>
<li>
le nombre \(a\) est une <strong>valeur approch�e
par exc�s</strong> de \(x\) � \(p\) pr�s, ou � la pr�cision \(p\), lorsque \(0\leqslant a-x \leqslant p\)
c'est-�-dire lorsque  \(a-p\leqslant x\leqslant a \).
</li>
</ul>
</div>
