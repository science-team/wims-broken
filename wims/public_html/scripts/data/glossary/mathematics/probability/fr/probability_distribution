!!abstract,linked gloses,internal links,content,dynamic examples,...
!set gl_author=Euler, Acad�mie de Versailles
!set gl_keywords=probability
!set gl_title=Loi de probabilit�
!set gl_level=H4
:
:
:
:
<div class="wims_defn"><h4>D�finitions</h4>
Soit <math xmlns='http://www.w3.org/1998/Math/MathML'>
 <mi fontstyle="normal">&#937;</mi>
</math> l'univers associ� � une exp�rience al�atoire.<br/>
On suppose <math xmlns='http://www.w3.org/1998/Math/MathML'>
 <mi fontstyle="normal">&#937;</mi>
</math> fini ; on note \(n\) le nombre d'�l�ments de
<math xmlns='http://www.w3.org/1998/Math/MathML'>
 <mi fontstyle="normal">&#937;</mi>
</math> (\(n\) est un entier naturel non nul appel� �galement cardinal de <math xmlns='http://www.w3.org/1998/Math/MathML'>
 <mi fontstyle="normal">&#937;</mi>
</math>) et
<math xmlns='http://www.w3.org/1998/Math/MathML'>
 <mrow>
  <msub>
   <mi>x</mi>
   <mn>1</mn>
  </msub>
 </mrow></math>, <math xmlns='http://www.w3.org/1998/Math/MathML'>
 <mrow>
  <msub>
   <mi>x</mi>
   <mn>2</mn>
  </msub>
 </mrow></math>, &#8230; , <math xmlns='http://www.w3.org/1998/Math/MathML'>
 <mrow>
  <msub>
   <mi>x</mi>
   <mi>n</mi>
  </msub>
 </mrow>
</math>
 les �l�ments de
<math xmlns='http://www.w3.org/1998/Math/MathML'>
 <mi fontstyle="normal">&#937;</mi>
</math>.<br/>
D�finir une <strong>loi de probabilit�</strong> sur
<math xmlns='http://www.w3.org/1998/Math/MathML'>
 <mi fontstyle="normal">&#937;</mi></math>,
 c'est associer � chaque �v�nement �l�mentaire
 <math xmlns='http://www.w3.org/1998/Math/MathML'>
 <mrow>
  <mrow>
   <mo>{</mo>
   <msub>
    <mi>x</mi>
    <mi>i</mi>
   </msub>
   <mo>}</mo>
  </mrow>
 </mrow>
</math>
 (\(i\) entier naturel compris entre 1 et \(n\))
un nombre r�el <math xmlns='http://www.w3.org/1998/Math/MathML'>
 <mrow>
  <msub>
   <mi>p</mi>
   <mi>i</mi>
  </msub>
 </mrow>
</math> positif ou nul
de fa�on que :

<math xmlns='http://www.w3.org/1998/Math/MathML' display='block'>
 <mrow>

   <mo>&#8290;</mo>
   <munderover>
    <mrow>
     <mtext> </mtext>
     <mstyle scriptlevel='0'>
      <mo>&#8721;</mo>
     </mstyle>
    </mrow>
    <mrow>
     <mi>i</mi>
     <mo>=</mo>
     <mn>1</mn>
    </mrow>
    <mi>n</mi>
   </munderover>
   <mo>&#8290;</mo>
   <msub>
    <mi>p</mi>
    <mi>i</mi>
   </msub>
  <mo>=</mo><mn>1</mn>
 </mrow>
</math>
  <br/>
Le nombre
<math xmlns='http://www.w3.org/1998/Math/MathML'>
 <mrow>
  <msub>
   <mi>p</mi>
   <mi>i</mi>
  </msub>
 </mrow>
</math> est appel� <strong>probabilit�</strong> de l'�v�nement �l�mentaire  <math xmlns='http://www.w3.org/1998/Math/MathML'>
 <mrow>
  <mrow>
   <mo>{</mo>
   <msub>
    <mi>x</mi>
    <mi>i</mi>
   </msub>
   <mo>}</mo>
  </mrow>
 </mrow>
</math>.

</div>
<div class="wims_rem">
Pour tout entier naturel \(i\) tel que \(1 \leqslant i \leqslant n\), on a \(0 \leqslant p_i \leqslant 1\).</div>


