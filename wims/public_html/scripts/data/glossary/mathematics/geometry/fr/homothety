!!abstract,linked gloses,internal links,content,dynamic examples,...
!set gl_author=Euler, Acad�mie de Versailles
!set gl_keywords=
!set gl_title=Homoth�tie
:
:
:
:
<div class="wims_defn"><h4>D�finition</h4>
Soit \(I\) un point du plan ou de l'espace et \(k\)
un nombre r�el non nul.
<br/>
L'<strong>homoth�tie</strong> de centre \(I\) et de rapport \(k\)
 est la transformation (du plan ou de l'espace) par laquelle tout point \(M\) a pour image le point
 <math xmlns='http://www.w3.org/1998/Math/MathML'>
 <mrow>
  <msup>
   <mi fontstyle='normal'>M</mi>
   <mo>&#8242;</mo>
  </msup>
 </mrow>
</math>
 tel que
 <math xmlns='http://www.w3.org/1998/Math/MathML' >
 <mrow>
  <mover>
   <msup>
    <mi>IM</mi>
    <mo>&#8242;</mo>
   </msup>
   <semantics>
    <mo>&#8594;</mo>

   </semantics>
  </mover>
  <mo>=</mo>
  <mi>k</mi>
  <mover>
   <mi>IM</mi>
   <semantics>
    <mo>&#8594;</mo>

   </semantics>
  </mover>
 </mrow>
</math>.
</div>
 <div class="wims_rem">
 <h4>Cas particuliers :</h4>
<ul>
<li>Une homoth�tie de rapport 1 est l'identit�, tout point est invariant.</li>
<li>Une homoth�tie de rapport <math xmlns='http://www.w3.org/1998/Math/MathML'>
 <mrow>
  <mo>-</mo>
  <mn>1</mn>
 </mrow>
</math> est une sym�trie centrale.</li></ul>

<div class="wims_thm"><h4>Th�or�me</h4>
Si
<math xmlns='http://www.w3.org/1998/Math/MathML'>
 <mi>h</mi>
</math>
est une homoth�tie de rapport
<math xmlns='http://www.w3.org/1998/Math/MathML'>
 <mi>k</mi>
</math>
et si
M, N, M', N' sont quatre points du plan ou de l'espace tels que
  <math xmlns='http://www.w3.org/1998/Math/MathML'>
 <mrow>
  <mrow>
   <mi>h</mi>

   <mo>(</mo>
   <mi fontstyle='normal'>M</mi>
   <mo>)</mo>
  </mrow>
  <mo>=</mo>
  <msup>
   <mi fontstyle='normal'>M</mi>
   <mo>&#8242;</mo>
  </msup>
 </mrow>
</math> et
<math xmlns='http://www.w3.org/1998/Math/MathML'>
 <mrow>
  <mrow>
   <mi>h</mi>

   <mo>(</mo>
   <mi fontstyle='normal'>N</mi>
   <mo>)</mo>
  </mrow>
  <mo>=</mo>
  <msup>
   <mi fontstyle='normal'>N</mi>
   <mo>&#8242;</mo>
  </msup>
 </mrow>
</math>, alors
<math xmlns='http://www.w3.org/1998/Math/MathML' >
 <mrow>
  <mover>
   <mrow>
    <msup>
     <mi fontstyle='normal'>M</mi>
     <mo>&#8242;</mo>
    </msup>
    <mo>&#8290;</mo>
    <msup>
     <mi fontstyle='normal'>N</mi>
     <mo>&#8242;</mo>
    </msup>
   </mrow>
   <mo>&#8594;</mo>
  </mover>
  <mo>=</mo>
  <mrow>
   <mi>k</mi>
   <mo>&#8290;</mo>
   <mover>
    <mi>MN</mi>
    <mo>&#8594;</mo>
   </mover>
  </mrow>
 </mrow>
</math>.
</div>
:mathematics/geometry/fr/homothety_1