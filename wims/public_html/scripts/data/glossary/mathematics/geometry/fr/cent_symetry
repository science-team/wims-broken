!!abstract,linked gloses,internal links,content,dynamic examples,...
!set gl_author=Euler, Acad�mie de Versailles
!set gl_keywords=
!set gl_title=Sym�trie centrale
:
:
:
:
<div class="wims_defn"><h4>D�finition</h4>
Soit \(I\) un point du plan<!-- ou de l'espace-->.<br/>
La <strong>sym�trie centrale</strong> de centre \(I\) est la transformation laissant \(I\)
invariant et par laquelle tout point
  \(M\) distinct de \(I\) a pour image le point
 <math xmlns='http://www.w3.org/1998/Math/MathML'>
 <mrow>
  <msup>
   <mi fontstyle='normal'>M</mi>
   <mo>&#8242;</mo>
  </msup>
 </mrow>
</math>
 tel que \(I\) est le milieu du segment
 <math xmlns='http://www.w3.org/1998/Math/MathML'>
 <mrow>
  <mo>[</mo>
  <mrow>
   <mi fontstyle='normal'>M</mi>
  <msup>
   <mi fontstyle='normal'>M</mi>
   <mo>&#8242;</mo>
  </msup>
  </mrow>
  <mo>]</mo>
 </mrow>
</math>.</div>
<div class="wims_thm">
 <h4>Propri�t�</h4>

 Le seul point invariant par une sym�trie centrale est son centre.

</div>
:mathematics/geometry/fr/cent_symetry_1
:
<div class="wims_thm"><h4>Th�or�me</h4>
Une sym�trie centrale conserve les distances : si
<math xmlns='http://www.w3.org/1998/Math/MathML'>
 <mi>s</mi>
</math>
 est une sym�trie centrale et si \(A\), \(B\), \(A'\), \(B'\) sont quatre points tels que
  <math xmlns='http://www.w3.org/1998/Math/MathML'>
 <mrow>
  <mrow>
   <mi>s</mi>
   <mo>(</mo>
   <mi fontstyle='normal'>A</mi>
   <mo>)</mo>
  </mrow>
  <mo>=</mo>
  <msup>
   <mi fontstyle='normal'>A</mi>
   <mo>&#8242;</mo>
  </msup>
 </mrow>
</math> et
<math xmlns='http://www.w3.org/1998/Math/MathML'>
 <mrow>
  <mrow>
   <mi>s</mi>
   <mo>(</mo>
   <mi fontstyle='normal'>B</mi>
   <mo>)</mo>
  </mrow>
  <mo>=</mo>
  <msup>
   <mi fontstyle='normal'>B</mi>
   <mo>&#8242;</mo>
  </msup>
 </mrow>
</math>, alors
  <math xmlns='http://www.w3.org/1998/Math/MathML'>
 <mrow>
  <mrow>
   <msup>
    <mi fontstyle='normal'>A</mi>
    <mo>&#8242;</mo>
   </msup>
   <msup>
    <mi fontstyle='normal'>B</mi>
    <mo>&#8242;</mo>
   </msup>
  </mrow>
  <mo>=</mo>
  <mi>AB</mi>
 </mrow>
</math>.
</div>
:mathematics/geometry/fr/cent_symetry_2
:
<div class="wims_thm"><h4>Th�or�me</h4>
Le quadrilat�re ABCD est un parall�logramme si et seulement si la sym�trie centrale qui transforme \(A\) en \(C\), transforme \(B\) en \(D\).
</div>
:mathematics/geometry/fr/cent_symetry_3
