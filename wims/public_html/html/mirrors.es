# This file contains the list of publically accessible mirror sites of Wims.
#
# Format:
# Line 1	URL address
# Line 2	Plain text site name
# Line 3	Country
# Line 4	Supported languages
# Line 5	Site manager name
# Line 6	Site manager email
# Line 7	Comments

:wims.unice.fr/wims/
Universite de Nice
France
en fr cn it es nl
BADO, Olivier
bado@unice.fr
Sitio principal de WIMS
:https://wims.auto.u-psud.fr/wims/
Universite Paris-Sud
France
fr en cn es it
wims@wims.auto.u-psud.fr
wims@wims.auto.u-psud.fr
:wims.matapp.unimib.it/wims/
Universit� di Milano--Bicocca
Italy
it en fr es
Marina Cazzola
marina.cazzola@unimib.it
:wims.unicaen.fr/wims/
Caen University
France
en fr cn es it nl
Eric Reyssat
reyssat@math.unicaen.fr
:http://insbaixpenedes.dyndns.org/wims/
Institut Baix Pened�s. El Vendrell
Spain
ca en fr es it nl
Manel Querol
mquerol@xtec.cat
